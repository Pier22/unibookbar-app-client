export class Client {
    UuidClient: string;
    SerialNumber: string;
    Name: string;
    Surname: string;
    Password: string;
    Email: string;
    Username: string;
    UrlImage: string;


    constructor(uuidclient: string, serialnumber: string, name: string, surname: string, password: string,
                email: string, username: string, urlimage: string)
    {
        this.UuidClient = uuidclient;
        this.SerialNumber = serialnumber;
        this.Name = name;
        this.Surname = surname;
        this.Password = password;
        this.Email = email;
        this.Username = username;
        this.UrlImage = urlimage;
    }
}
