export class Dish {
    UuidDish: string;
    Name: string;
    Unit: number;
    Price: number;
    BkbleConble: boolean;
    UrlImage: string;


    constructor(uuiddish: string, name: string, unit: number, price: number, bkbleconble: boolean, urlimage: string) {
        this.UuidDish = uuiddish;
        this.Name = name;
        this.Unit = unit;
        this.Price = price;
        this.BkbleConble = bkbleconble;
        this.UrlImage = urlimage;
    }
}
