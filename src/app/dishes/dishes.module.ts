import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {DishesPageRoutingModule} from './dishes-routing.module';
import {DishesPage} from "./dishes.component";
import {MenuClass} from "../menu.module";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DishesPageRoutingModule,
        MenuClass
    ],
    declarations: [DishesPage],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class DishesPageModule {
}
