import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CartModalPage} from "../cart-modal/cart-modal.component";
import {DishService} from "../services/dish.service";
import {ModalController} from "@ionic/angular";
import {DishDetailsPage} from "../dish-details/dish-details.page";
import {environment} from "../../environments/environment";

@Component({
    selector: 'app-dishes',
    templateUrl: './dishes.component.html',
    styleUrls: ['./dishes.component.scss'],
})
export class DishesPage implements OnInit {
    dishes = [];
    cart = [];
    cartItemCount: number = 0;
    category: string = environment.baseCategory;

    @ViewChild('cart', {static: false, read: ElementRef}) fab: ElementRef;

    constructor(public dishService: DishService, private modalCtrl: ModalController) {

    }

    ngOnInit() {
        this.dishService.cart = undefined;
        this.dishService.totalItems = 0;
        this.dishService.getDishes();
        this.cart = this.getCart();
        this.cartItemCount = this.getCartItemCount();
    }

    getDishesByCategory(category: string) {
        this.category = category;
    }

    addToCart(dish) {
        this.dishService.totalItems += 1;
        let added = false;
        for (let d of this.cart) {
            if (d.UuidDish === dish.UuidDish) {
                d.dish.Unit += 1;
                added = true;
                break;
            }
        }
        if (!added) {
            dish.dish.Unit = 1;
            this.cart.push(dish);
            this.dishService.pushOnCart(this.cart);
        }

        this.cartItemCount += 1;

        this.animateCSS('tada');
    }

    getCartItemCount() {
        return this.dishService.totalItems;
    }

    getCart() {
        return this.cart;
    }

    async openCart() {
        this.animateCSS('bounceOutLeft', true);

        let modal = await this.modalCtrl.create({
            component: CartModalPage,
            cssClass: 'my-custom-modal-css'
        });
        modal.onWillDismiss().then(() => {
            this.fab.nativeElement.classList.remove('animated', 'bounceOutLeft');
            this.animateCSS('bounceInLeft');
        });
        modal.present();
    }

    async modalDishDetails() {
        const modal = await this.modalCtrl.create({
            component: DishDetailsPage,
            cssClass: 'my-custom-modal-css'
        });
        return await modal.present();
    }

    getDishDetails(dish){
        this.dishService.getDishDetails(dish);
    }

    animateCSS(animationName, keepAnimated = false) {
        const node = this.fab.nativeElement;
        node.classList.add('animated', animationName);

        function handleAnimationEnd() {
            if (!keepAnimated) {
                node.classList.remove('animated', animationName);
            }
            node.removeEventListener('animationend', handleAnimationEnd);
        }

        node.addEventListener('animationend', handleAnimationEnd);
    }

}
