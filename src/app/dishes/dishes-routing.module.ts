import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DishesPage} from "./dishes.component";
import {AuthGuard} from "../auth.guard";


const routes: Routes = [
  {
    path: '',
    component: DishesPage , canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DishesPageRoutingModule {}
