import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import Swal from "sweetalert2";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class RewiewService {
    apiUrl = environment.apiUrl;

    public reviews: any;
    public data = [];
    selected: boolean = false;

    constructor(public http: HttpClient, public router: Router) {
    }

    getAllReservations() {
        this.http.get(this.apiUrl + 'clients/show_all_reservation').subscribe((res: any) => {
            if (res) {
                this.reviews = res;
            }
        });
    }

    sendRewiew(body: any) {
        this.http.post(this.apiUrl + 'clients/write_review', body).subscribe((res: any) => {
            if (res.code === 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: res.message
                });
                this.selected = false;
            }
        }, error => {
            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            })
        });
    }


    getReview() {
        this.http.get(this.apiUrl + 'clients/show_reviews').subscribe((res: any) => {
            if (res) {
                this.data = res;
                if (res['length'] == 0) {
                    this.data = undefined;
                }
            }
        });
    }

    deleteReview(uuid_rev) {
        this.http.post(this.apiUrl + 'clients/delete_review', {
            uuid_rev
        }).subscribe((res: any) => {
            if (res.code === 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: res.message
                });
                for (let i = 0; i < this.data.length; i++) {
                    if (this.data[i]['UuidRev'] == uuid_rev) {
                        this.data.splice(i, 1);
                    }
                }
            }

        }, error => {
            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            })
        });
    }


    back_to_list() {
            this.selected = false;
    }
}
