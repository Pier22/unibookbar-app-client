import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ModalController} from "@ionic/angular";
import Swal from "sweetalert2";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class DishService {
    apiUrl = environment.apiUrl;

    quantity = [];
    data: any;
    dish_details: any;
    cart: any;
    order: any;
    totalItems: number = 0;

    prenotation = [];
    prenotation_to_modify: any;


    constructor(public http: HttpClient, private router: Router, private modalCtrl: ModalController) {
    }


    getDishes(): any {
        this.http.get(this.apiUrl + 'clients/all_dishes').subscribe((res: any) => {
            if (res) {
                this.data = res;
            }
        });
    }

    pushOnCart(pushedCart: any) {
        this.cart = pushedCart;
    }

    sendPrenotation() {
        let uuid_dish = [];
        let quantity = [];
        this.createArrayCart();

        for (let i = 0; i < this.order.length; i++) {
            uuid_dish.push(this.order[i].UuidDish);
            quantity.push(this.order[i].dish.Unit);
        }

        this.http.post(this.apiUrl + 'clients/dish_reservation', {
            uuid_dish, quantity
        }).subscribe((res: any) => {
            if (res.code === 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });

                Toast.fire({
                    icon: 'success',
                    title: res.message
                });
                this.modalCtrl.dismiss();
            }
        }, error => {
            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            });
            this.cart.splice(0, this.cart['length']);
            this.modalCtrl.dismiss();
        });
        this.totalItems = 0;
    }


    removeToCart(i) {
        this.quantity.splice(i, 1);
        this.cart.splice(i, 1);
    }

    calcolateTotal(): number {
        let total: number = 0;
        for (let i = 0; i < this.cart.length; i++) {
            total += this.cart[i]['Dish']['Price'] * this.quantity[i];
        }
        return total;
    }


    createArrayCart(): any {
        let cart = [];
        for (let i = 0; i < this.cart.length; i++) {
            cart[i] = this.cart[i];
        }
        this.order = cart;

        return this.cart;
    }

    updateQuantity(quantity: number, d) {
        this.quantity[this.cart.indexOf(d)] = quantity;

    }

    showActivePrenotation() {
        this.http.get(this.apiUrl + 'clients/my_reservations').subscribe((res: any) => {
            if (res) {
                this.prenotation = res;
                if (res['length'] == 0) {
                    this.prenotation = undefined;
                }
            }
        });
    }

    deletePrenotation(uuid_reserv) {
        this.http.post(this.apiUrl + 'clients/delete_all_reservation', {
            uuid_reserv
        }).subscribe((res: any) => {
            if (res.code === 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: 'Cancellazione effettuata'
                });
                for (let i = 0; i < this.prenotation.length; i++) {
                    if (this.prenotation[i]['uuidReserv'] == uuid_reserv) {
                        this.prenotation.splice(i, 1);
                    }
                }
            }
        }, error => {
            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            })
        });
    }


    getDishDetails(dish) {
        this.http.get(this.apiUrl + 'clients/detail_dish?uuid_dish=' + dish['UuidDish']).subscribe((res: any) => {
            if (res) {
                this.dish_details = res;
            }
        });
    }

    sendEditPrenotation(uuid_reserv, uuid_dish, quantity) {
        this.http.post(this.apiUrl + 'clients/edit_reservation', {
            uuid_reserv,
            uuid_dish,
            quantity
        }).subscribe((res: any) => {
            if (res.code === 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: res.message
                });
                this.modalCtrl.dismiss();
            }
        }, error => {

            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            }).then(function () {
               window.location.href='/my-prenotations'
            })

        });
    }

    sendDeletePrenotation(uuid_dish, uuid_reserv) {
        this.http.post(this.apiUrl + 'clients/delete_dishes_reservation', {
            uuid_dish,
            uuid_reserv,
        }).subscribe((res: any) => {
            if (res.code === 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: res.message
                });
                this.modalCtrl.dismiss();
            }
        }, error => {

            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            }).then(function () {
                window.location.href='/my-prenotations'
            })


        });
    }

}
