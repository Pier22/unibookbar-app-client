import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from "rxjs";
import {Platform} from "@ionic/angular";
import {Storage} from "@ionic/storage";
import {Router} from "@angular/router";
import Swal from 'sweetalert2'
import {environment} from "../../environments/environment";

const TOKEN_KEY = 'auth_token';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    apiUrl = environment.apiUrl;


    data: any;
    auth_state = new BehaviorSubject(false);

    constructor(public httpClient: HttpClient, public storage: Storage, private plt: Platform, private router: Router) {
        this.plt.ready().then(() => {
            this.check_token();
        })
    }

    login(email: string, password: string) {
        return this.httpClient.post(this.apiUrl + 'clients/login', {
            email,
            password
        }).subscribe((res: any) => {
            if (res.code === 200) {
                localStorage.setItem('ACCESS_TOKEN', res['token']);
                this.router.navigate(['near_places'],{replaceUrl:true});
                this.auth_state.next(true);
            }
        }, error => {
            if (error['status'] == 400) {
                localStorage.setItem('ACCESS_TOKEN', null);
                Swal.fire({
                    title: error['error']['message'],
                    heightAuto: false,
                    icon: "error"
                })
            } else {
                Swal.fire({
                    title: 'Email o password non esistente',
                    text: 'Verifica di aver inserito email o password valide',
                    heightAuto: false,
                    icon: "error"
                })
            }
        });
    }

    check_token() {
        return this.storage.get(TOKEN_KEY).then(res => {
            if (res) {
                this.auth_state.next(true);
            }
        });
    }

    signup(email: string, password: string, confirmpassword: string, username: string, role: string) {


        return this.httpClient.post(this.apiUrl + 'clients/signup', {
            email,
            password,
            confirmpassword,
            username,
            role
        }).subscribe((res: any) => {
            if (res.code === 200) {
                localStorage.setItem('ACCESS_TOKEN', res['token']);
                this.router.navigate(['near_places'],{replaceUrl:true});
                this.auth_state.next(true);
            }
        }, error => {
            if (error['status'] === 400) {
                Swal.fire({
                    title: error['error']['message'],
                    heightAuto: false,
                    icon: "error"
                })
            } else {
                Swal.fire({
                    title: 'Email o password non esistente',
                    text: 'Verifica di aver inserito email o password valide',
                    heightAuto: false,
                    icon: "error"
                })
            }
        });
    }
}
