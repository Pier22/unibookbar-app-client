import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Md5} from 'ts-md5/dist/md5';
import Swal from "sweetalert2";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class ClientService {
    apiUrl = environment.apiUrl;

    data: any;
    password_modified = false;

    constructor(public httpClient: HttpClient, private router: Router) {
    }

    showAccount() {
        this.httpClient.get(this.apiUrl + 'clients/show_account').subscribe((res: any) => {
            if (res) {
                this.data = res;
            }
        });
    }

    modify_account(name, surname, username, password, url_image) {
        let body;
        if (this.password_modified) {
            password = this.data['Password'];
            body = {name, surname, username, password, url_image};
        } else {
            body = {name, surname, username, url_image};
        }

        this.httpClient.post(this.apiUrl + 'clients/edit_account', body).subscribe((res: any) => {
            if (res.code === 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });

                Toast.fire({
                    icon: 'success',
                    title: res.message
                });
                if (this.password_modified) {
                    this.data['Password'] = Md5.hashStr(password);
                }
                this.password_modified = false;
                this.router.navigate(['/home-place'],{replaceUrl:true});
            }
        }, error => {
            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            })
        });
    }

    verifyOldPassword(password: string): boolean {
        if (this.password_modified == true) {
            return (password) == this.data['Password'];
        } else {
            return Md5.hashStr(password) == this.data['Password'];
        }
    }

    verifyNewPassword(password: string, confirm_password: string): boolean {
        return password == confirm_password && password.length != 0 && confirm_password.length != 0;
    }

    modifyPassword(password) {
        this.data['Password'] = password;
        this.password_modified = true;
    }
}
