import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import Swal from "sweetalert2";
import {environment} from "../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class TableService {
    apiUrl = environment.apiUrl;

    data: any;
    tables = [];
    current_prenotation = [];

    constructor(public http: HttpClient, public httpClient: HttpClient) {
    }

    getFreeTable() {
        this.http.get(this.apiUrl + 'clients/show_free_tables').subscribe((res: any) => {
            if (res) {
                this.data = res;
                if (res['length'] == 0) {
                    this.data = undefined;
                }
            }
        });
    }

    sendReservationTable(uuid_table: string) {
        return this.httpClient.post(this.apiUrl + 'clients/occupy_table', {
            uuid_table
        }).subscribe((res: any) => {
            if (res.code === 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: res.message
                });
            }
        }, error => {
            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            })
        });
    }

    getMyTable() {
        let today = new Date();
        let date: any;
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        date = yyyy + '-' + mm + '-' + dd;

        this.http.get(this.apiUrl + 'clients/show_tables').subscribe((res: any) => {
            if (res) {
                this.tables = res;
                for (let i = 0; i < this.tables.length; i++) {
                    if (this.tables[i]['Date'] == date) {
                        this.current_prenotation.push(this.tables[i]);
                        this.tables.splice(i, 1);
                    }
                }
                if (res['length'] == 0) {
                    this.tables = undefined;
                }
            }
        });
    }

    deleteTable(uuid_occupy) {
        this.http.post(this.apiUrl + 'clients/delete_occupation', {
            uuid_occupy
        }).subscribe((res: any) => {
            if (res.code === 200) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: res.message
                });
                this.current_prenotation = undefined;
            }
        }, error => {
            Swal.fire({
                title: error['error']['message'],
                heightAuto: false,
                icon: "error"
            })
        });
    }
}
