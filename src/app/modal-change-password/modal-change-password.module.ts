import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalChangePasswordPageRoutingModule } from './modal-change-password-routing.module';

import { ModalChangePasswordPage } from './modal-change-password.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalChangePasswordPageRoutingModule
  ],
  declarations: [ModalChangePasswordPage]
})
export class ModalChangePasswordPageModule {}
