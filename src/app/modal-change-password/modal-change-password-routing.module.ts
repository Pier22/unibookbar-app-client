import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalChangePasswordPage } from './modal-change-password.page';

const routes: Routes = [
  {
    path: '',
    component: ModalChangePasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalChangePasswordPageRoutingModule {}
