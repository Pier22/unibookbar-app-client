import {Component, OnInit} from '@angular/core';
import {ModalController} from "@ionic/angular";
import {ClientService} from "../services/client.service";

@Component({
  selector: 'app-modal-change-password',
  templateUrl: './modal-change-password.page.html',
  styleUrls: ['./modal-change-password.page.scss'],
})

export class ModalChangePasswordPage implements OnInit {

  verifyPassword;
  constructor(public modalController: ModalController, public clientService: ClientService) {
  }

  ngOnInit() {
  }

  verifyOldPassword(password: string){
    this.verifyPassword = this.clientService.verifyOldPassword(password)
  }

  verifyNewPassword(password: string, confirm_password: string): boolean {
    return this.clientService.verifyNewPassword(password, confirm_password);
  }

  modifyPassword(password) {
    this.clientService.modifyPassword(password);
    this.modalController.dismiss();
  }
}
