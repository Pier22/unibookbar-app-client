import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NearplacesPage } from './nearplaces.page';

const routes: Routes = [
  {
    path: '',
    component: NearplacesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NearplacesPageRoutingModule {}
