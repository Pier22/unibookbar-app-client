import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NearplacesPageRoutingModule } from './nearplaces-routing.module';

import { NearplacesPage } from './nearplaces.page';
import {MenuClass} from "../menu.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NearplacesPageRoutingModule,
        MenuClass
    ],
  declarations: [NearplacesPage]
})
export class NearplacesPageModule {}
