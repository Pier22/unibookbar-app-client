import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-nearplaces',
    templateUrl: './nearplaces.page.html',
    styleUrls: ['./nearplaces.page.scss'],
})
export class NearplacesPage implements OnInit {

    img_local1 = 'assets/img/tattira.png';
    img_local2 = 'assets/img/barunimol.png';

    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    public goTo(path: string) {
        this.router.navigate([path],{replaceUrl:true});
    }
}
