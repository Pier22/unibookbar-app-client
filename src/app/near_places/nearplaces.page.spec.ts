import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NearplacesPage } from './nearplaces.page';

describe('NearplacesPage', () => {
  let component: NearplacesPage;
  let fixture: ComponentFixture<NearplacesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NearplacesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NearplacesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
