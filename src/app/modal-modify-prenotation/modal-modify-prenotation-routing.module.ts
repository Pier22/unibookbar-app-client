import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalModifyPrenotationPage } from './modal-modify-prenotation.page';

const routes: Routes = [
  {
    path: '',
    component: ModalModifyPrenotationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalModifyPrenotationPageRoutingModule {}
