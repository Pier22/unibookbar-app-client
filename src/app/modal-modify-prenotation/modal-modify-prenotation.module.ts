import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalModifyPrenotationPageRoutingModule } from './modal-modify-prenotation-routing.module';

import { ModalModifyPrenotationPage } from './modal-modify-prenotation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalModifyPrenotationPageRoutingModule
  ],
  declarations: [ModalModifyPrenotationPage]
})
export class ModalModifyPrenotationPageModule {}
