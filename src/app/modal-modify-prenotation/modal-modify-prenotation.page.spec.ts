import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalModifyPrenotationPage } from './modal-modify-prenotation.page';

describe('ModalModifyPrenotationPage', () => {
  let component: ModalModifyPrenotationPage;
  let fixture: ComponentFixture<ModalModifyPrenotationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalModifyPrenotationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalModifyPrenotationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
