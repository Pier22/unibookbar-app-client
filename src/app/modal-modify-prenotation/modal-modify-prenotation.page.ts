import {Component, OnInit} from '@angular/core';
import {DishService} from "../services/dish.service";
import {ModalController} from "@ionic/angular";

@Component({
    selector: 'app-modal-modify-prenotation',
    templateUrl: './modal-modify-prenotation.page.html',
    styleUrls: ['./modal-modify-prenotation.page.scss'],
})
export class ModalModifyPrenotationPage implements OnInit {
    prenotation: any;

    copy_quantity_prenotation: any = [];
    copy_dish_prenotation: any = [];
    copy_uuid_dish_prenotation: any = [];
    copy_price_prenotation: any = [];
    uuid_reserv: any;

    deleted_dish: any = [];


    constructor(public dishService: DishService, private modalCtrl: ModalController) {
    }

    ngOnInit() {
        this.prenotation = this.dishService.prenotation_to_modify;
        for (let i = 0; i < this.prenotation['quantity1'].length; i++) {

            this.copy_quantity_prenotation.push(this.prenotation['quantity1'][i]);
            this.copy_dish_prenotation.push(this.prenotation['dish'][i]);
            this.copy_price_prenotation.push(this.prenotation['price'][i]);
            this.copy_uuid_dish_prenotation.push(this.prenotation['uuid_dish'][i]);
        }
        this.uuid_reserv = this.prenotation['uuidReserv'];
    }

    decreaseCartItem(i) {
        if (this.copy_quantity_prenotation[i] != 1) {
            this.copy_quantity_prenotation[i] -= 1;
        }
    }

    increaseCartItem(i) {
        this.copy_quantity_prenotation[i] += 1;
    }

    removeCartItem(i) {
        this.deleted_dish.push(this.prenotation['uuid_dish'][i]);

        this.copy_dish_prenotation.splice(i, 1);
        this.copy_quantity_prenotation.splice(i, 1);
        this.copy_price_prenotation.splice(i, 1);
        this.copy_uuid_dish_prenotation.splice(i, 1);
    }

    getTotal(prices: [], quantities: []) {
        let total = 0;

        for (let i = 0; i < prices.length; i++) {
            total += prices[i] * quantities[i];
        }
        return total
    }

    close() {
        this.modalCtrl.dismiss();
    }

    checkout() {
        this.dishService.sendDeletePrenotation(this.deleted_dish, this.uuid_reserv);

        if (this.copy_dish_prenotation.length > 0) {
            for (let i = 0; i < this.copy_dish_prenotation.length; i++) {
                this.dishService.sendEditPrenotation(this.prenotation['uuidReserv'], this.copy_uuid_dish_prenotation[i], this.copy_quantity_prenotation[i])
            }
        }

        for (let i = 0; i < this.dishService.prenotation.length; i++) {
            if (this.uuid_reserv == this.dishService.prenotation[i]['uuidReserv']) {
                this.dishService.prenotation[i]['quantity1'] = this.copy_quantity_prenotation;
                this.dishService.prenotation[i]['name'] = this.copy_dish_prenotation;
                this.dishService.prenotation[i]['dish'] = this.copy_dish_prenotation;
                this.dishService.prenotation[i]['price'] = this.copy_price_prenotation;
            }
            if (this.dishService.prenotation[i]['dish'].length == 0) {
                this.dishService.prenotation.splice(i, 1);
            }
        }
    }
}
