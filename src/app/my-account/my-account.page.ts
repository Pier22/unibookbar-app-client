import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ClientService} from "../services/client.service";
import {ModalController} from "@ionic/angular";
import {ModalChangePasswordPage} from "../modal-change-password/modal-change-password.page";


@Component({
    selector: 'app-my-account',
    templateUrl: './my-account.page.html',
    styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage implements OnInit {
    img_account = 'assets/img/Microsoft_Account.svg.png';
    name: string;

    constructor(private router: Router, public client_service: ClientService, public modalController: ModalController) {
    }

    ngOnInit() {
        this.client_service.showAccount();
    }

    public goTo(path: string) {
        this.router.navigate([path],{replaceUrl:true});
    }

    async modalChangePassword() {

        const modal = await this.modalController.create({
            component: ModalChangePasswordPage,
            cssClass: 'my-custom-modal-css'
        });
        return await modal.present();
    }

    modify_account(name, surname, username, password, url_image) {
        this.client_service.modify_account(name, surname, username, password, url_image);
    }


}
