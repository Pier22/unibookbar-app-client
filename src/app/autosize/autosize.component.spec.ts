import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AutosizeComponent } from './autosize.component';

describe('AutosieComponent', () => {
  let component: AutosizeComponent;
  let fixture: ComponentFixture<AutosizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutosizeComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AutosizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
