import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyPrenotationsPageRoutingModule } from './my-prenotations-routing.module';

import { MyPrenotationsPage } from './my-prenotations.page';
import {MenuClass} from "../menu.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MyPrenotationsPageRoutingModule,
        MenuClass
    ],
  declarations: [MyPrenotationsPage]
})
export class MyPrenotationsPageModule {}
