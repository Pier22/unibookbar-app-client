import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyPrenotationsPage } from './my-prenotations.page';

describe('MyPrenotationsPage', () => {
  let component: MyPrenotationsPage;
  let fixture: ComponentFixture<MyPrenotationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPrenotationsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyPrenotationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
