import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyPrenotationsPage } from './my-prenotations.page';

const routes: Routes = [
  {
    path: '',
    component: MyPrenotationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyPrenotationsPageRoutingModule {}
