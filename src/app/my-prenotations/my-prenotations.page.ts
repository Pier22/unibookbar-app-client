import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {Router} from "@angular/router";
import {DishService} from "../services/dish.service";
import {ModalModifyPrenotationPage} from "../modal-modify-prenotation/modal-modify-prenotation.page";


@Component({
    selector: 'app-my-prenotations',
    templateUrl: './my-prenotations.page.html',
    styleUrls: ['./my-prenotations.page.scss'],
})

export class MyPrenotationsPage implements OnInit {

    constructor(public modalController: ModalController, private router: Router, public dishService: DishService) {
    }

    ngOnInit() {
        this.dishService.showActivePrenotation();
    }

    public goTo(path: string) {
        this.router.navigate([path],{replaceUrl:true});
    }

    async modalResumePrenotation(myres) {
        this.dishService.prenotation_to_modify = myres;
        const modal = await this.modalController.create({
            component: ModalModifyPrenotationPage,
            cssClass: 'my-custom-modal-css'
        });
        return await modal.present();
    }

    deletePrenotation(uuid_reserv) {
        this.dishService.deletePrenotation(uuid_reserv);
    }

}
