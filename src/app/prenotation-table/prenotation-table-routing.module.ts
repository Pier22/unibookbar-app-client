import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrenotationTablePage } from './prenotation-table.page';

const routes: Routes = [
  {
    path: '',
    component: PrenotationTablePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrenotationTablePageRoutingModule {}
