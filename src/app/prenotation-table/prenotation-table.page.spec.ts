import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PrenotationTablePage } from './prenotation-table.page';

describe('PrenotationTablePage', () => {
  let component: PrenotationTablePage;
  let fixture: ComponentFixture<PrenotationTablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrenotationTablePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PrenotationTablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
