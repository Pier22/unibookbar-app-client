import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {TableService} from "../services/table.service";

@Component({
    selector: 'app-prenotation-table',
    templateUrl: './prenotation-table.page.html',
    styleUrls: ['./prenotation-table.page.scss'],
})
export class PrenotationTablePage implements OnInit {

    img_table = 'assets/img/tavolo.jpg';

    constructor(private router: Router, public tableService: TableService) {
    }

    ngOnInit() {
        this.tableService.getMyTable();
        this.tableService.current_prenotation.splice(0, this.tableService.current_prenotation.length);
    }

    public goTo(path: string) {
        this.router.navigate([path],{replaceUrl:true});
    }

    deleteTable(uuid_occupy) {
        this.tableService.deleteTable(uuid_occupy);
    }

}
