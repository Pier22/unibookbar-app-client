import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrenotationTablePageRoutingModule } from './prenotation-table-routing.module';

import { PrenotationTablePage } from './prenotation-table.page';
import {MenuClass} from "../menu.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PrenotationTablePageRoutingModule,
        MenuClass
    ],
  declarations: [PrenotationTablePage]
})
export class PrenotationTablePageModule {}
