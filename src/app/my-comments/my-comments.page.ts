import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {RewiewService} from "../services/rewiew.service";

@Component({
    selector: 'app-my-comments',
    templateUrl: './my-comments.page.html',
    styleUrls: ['./my-comments.page.scss'],
})
export class MyCommentsPage implements OnInit {

    constructor(private router: Router, public rewiewService: RewiewService) {
    }

    ngOnInit() {
        this.rewiewService.getReview()
    }

    public goTo(path: string) {
        this.router.navigate([path],{replaceUrl:true});
    }

    presentAlert(title, text) {
        const alert = document.createElement('ion-alert');
        alert.header = title;
        alert.message = text;
        alert.buttons = ['OK'];
        document.body.appendChild(alert);
        return alert.present();
    }

    deleteReview(uuid_rev){
        this.rewiewService.deleteReview(uuid_rev);
    }
    selectStar(star, id_button): boolean{
        return star == id_button;
    }
}
