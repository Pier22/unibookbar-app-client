import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PrenotationPage } from './prenotation.page';

describe('PrenotationPage', () => {
  let component: PrenotationPage;
  let fixture: ComponentFixture<PrenotationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrenotationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PrenotationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
