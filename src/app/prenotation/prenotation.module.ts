import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrenotationPageRoutingModule } from './prenotation-routing.module';

import { PrenotationPage } from './prenotation.page';
import {MenuClass} from "../menu.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PrenotationPageRoutingModule,
        MenuClass
    ],
  declarations: [PrenotationPage]
})
export class PrenotationPageModule {}
