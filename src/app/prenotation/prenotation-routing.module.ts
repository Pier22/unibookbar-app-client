import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrenotationPage } from './prenotation.page';

const routes: Routes = [
  {
    path: '',
    component: PrenotationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrenotationPageRoutingModule {}
