import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import { ModalController} from "@ionic/angular";
import {DishService} from "../services/dish.service";

@Component({
    selector: 'app-prenotation',
    templateUrl: './prenotation.page.html',
    styleUrls: ['./prenotation.page.scss'],
})
export class PrenotationPage implements OnInit {

    img_dish = 'assets/img/spaghetti.jpg';

    constructor(private router: Router, private modalController: ModalController, public dishService: DishService) {
    }

    ngOnInit() {
    }

    public goTo(path: string) {
        this.router.navigate([path],{replaceUrl:true});
    }

    array_creator(i: number) {
        var qt = [];
        for (let j = 0; j <= i; j++) {
            qt[j] = j
        }
        return qt;
    }

    removeToCart(i) {
        this.dishService.removeToCart(i)
    }

    calcolateTotal(): number {
        return this.dishService.calcolateTotal()
    }

    public dismiss() {
        this.modalController.dismiss({
            'dismissed': true
        });
    }

    updateQuantity(quantity, d) {
        this.dishService.updateQuantity(quantity, d);
    }

    sendPrenotation() {
        this.dishService.sendPrenotation();
        this.dismiss();
        this.goTo('/home-place');
    }
}
