import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HistoryPrenotationDishPage } from './history-prenotation-dish.page';

describe('HistoryPrenotationTablePage', () => {
  let component: HistoryPrenotationDishPage;
  let fixture: ComponentFixture<HistoryPrenotationDishPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryPrenotationDishPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HistoryPrenotationDishPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
