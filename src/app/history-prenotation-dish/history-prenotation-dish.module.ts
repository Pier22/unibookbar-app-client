import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoryPrenotationTablePageRoutingModule } from './history-prenotation-dish-routing.module';

import { HistoryPrenotationDishPage } from './history-prenotation-dish.page';
import {MenuClass} from "../menu.module";
import {IonicRatingModule} from "ionic4-rating/dist";
import {AutosizeComponent} from "../autosize/autosize.component";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HistoryPrenotationTablePageRoutingModule,
        MenuClass,
        ReactiveFormsModule,
        IonicRatingModule
    ],
    exports: [
        AutosizeComponent
    ],
  declarations: [HistoryPrenotationDishPage , AutosizeComponent]
})
export class HistoryPrenotationTablePageModule {}
