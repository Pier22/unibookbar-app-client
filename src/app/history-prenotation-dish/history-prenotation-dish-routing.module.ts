import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoryPrenotationDishPage } from './history-prenotation-dish.page';

const routes: Routes = [
  {
    path: '',
    component: HistoryPrenotationDishPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoryPrenotationTablePageRoutingModule {}
