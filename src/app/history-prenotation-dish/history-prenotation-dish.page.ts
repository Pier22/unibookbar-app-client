import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {RewiewService} from "../services/rewiew.service";

@Component({
    selector: 'app-history-prenotation-table',
    templateUrl: './history-prenotation-dish.page.html',
    styleUrls: ['./history-prenotation-dish.page.scss'],
})
export class HistoryPrenotationDishPage implements OnInit {

    body: any;
    stars: number = 5;
    uuid: string;


    constructor(private router: Router, public rewiewService: RewiewService) {
    }

    ngOnInit() {
        this.rewiewService.getAllReservations();
    }

    public goTo(path: string) {
        this.router.navigate([path],{replaceUrl:true});
    }

    selectDish(uuidDish: string) {
        this.rewiewService.selected = true;
        this.uuid = uuidDish;
    }

    submit(title, text) {
        this.body = {
            "uuid_dish": this.uuid,
            "title": title,
            "star": this.stars,
            "text": text
        };
        this.rewiewService.sendRewiew(this.body)
    }

    onRateChange($event: number) {
        this.stars = $event;
    }
}
