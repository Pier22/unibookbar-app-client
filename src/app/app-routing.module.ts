import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from "./auth.guard";

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        // component:LoginPage
        loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
    },
    {
        path: 'signup',
        // component: SignupPage
        loadChildren: () => import('./signup/signup.module').then(m => m.SignupPageModule)
    },
    {
        path: 'near_places',
        // component: NearplacesPage, canActivate: [AuthGuard]
        loadChildren: () => import('./near_places/nearplaces.module').then(m => m.NearplacesPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'home-place',
        // component: HomePlacePage
        loadChildren: () => import('./home-place/home-place.module').then(m => m.HomePlacePageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'dish-details',
        // component: DishesPage
        loadChildren: () => import('./dish-details/dish-details.module').then(m => m.DishDetailsPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'free-table',
        // component: FreeTablePage
        loadChildren: () => import('./free-table/free-table.module').then(m => m.FreeTablePageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'prenotation-table',
        loadChildren: () => import('./prenotation-table/prenotation-table.module').then(m => m.PrenotationTablePageModule),
        canActivate: [AuthGuard]
        // component: PrenotationTablePage
    },
    {
        path: 'history-prenotation-dish',
        loadChildren: () => import('./history-prenotation-dish/history-prenotation-dish.module').then(m => m.HistoryPrenotationTablePageModule),
        canActivate: [AuthGuard]
        // component: HistoryPrenotationDishPage
    },
    {
        path: 'my-comments',
        loadChildren: () => import('./my-comments/my-comments.module').then(m => m.MyCommentsPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'my-account',
        // component: MyAccountPage,
        loadChildren: () => import('./my-account/my-account.module').then(m => m.MyAccountPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'my-prenotations',
        loadChildren: () => import('./my-prenotations/my-prenotations.module').then(m => m.MyPrenotationsPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'prenotation',
        loadChildren: () => import('./prenotation/prenotation.module').then(m => m.PrenotationPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'dishes',
        loadChildren: () => import('./dishes/dishes.module').then(m => m.DishesPageModule),
        canActivate: [AuthGuard]
    }, {
        path: 'cart-modal',
        loadChildren: () => import('./cart-modal/cart-modal.module').then(m => m.CartModalPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'modal-change-password',
        loadChildren: () => import('./modal-change-password/modal-change-password.module').then(m => m.ModalChangePasswordPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'modal-modify-prenotation',
        loadChildren: () => import('./modal-modify-prenotation/modal-modify-prenotation.module').then(m => m.ModalModifyPrenotationPageModule),
        canActivate: [AuthGuard]
    }

];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
