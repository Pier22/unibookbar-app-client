import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],

})

export class LoginPage implements OnInit {
    img = 'assets/img/Png copia 3.png';

    constructor(public auth_service: AuthService) {
    }

    ngOnInit() {
    }

    login(mail: any, password: any) {
        this.auth_service.login(mail, password);
    }
}
