import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-sidemenu',
    templateUrl: './sidemenu.component.html',
    styleUrls: ['./sidemenu.component.scss'],
})

export class SidemenuComponent implements OnInit {


    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    img_logo = 'assets/img/Png copia 3.png';

    public goTo(path: string) {
        this.router.navigate([path], {replaceUrl:true});
    }

    logout() {
        localStorage.removeItem('ACCESS_TOKEN');
        localStorage.removeItem('expires_at');
        localStorage.clear();
        this.router.navigate(['/login'],{replaceUrl:true});

    }
}
