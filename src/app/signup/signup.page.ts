import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Component({
    selector: 'app-signup',
    templateUrl: './signup.page.html',
    styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

    img = 'assets/img/Png copia 3.png';
    selected:string='0';

    constructor(private router: Router, public auth_service: AuthService) {
    }

    ngOnInit() {
    }

    signup(mail: any, password: any, confirmpassword: any, username: any, role: any) {
        this.auth_service.signup(mail, password, confirmpassword, username, role);
    }

    goToLogin() {
        this.router.navigate(['/login'],{replaceUrl:true});
    }
}
