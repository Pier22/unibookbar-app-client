import {Component, OnInit} from '@angular/core';
import {DishService} from "../services/dish.service";
import {AlertController, ModalController} from "@ionic/angular";

@Component({
    selector: 'app-cart-modal',
    templateUrl: './cart-modal.component.html',
    styleUrls: ['./cart-modal.component.scss'],
})
export class CartModalPage implements OnInit {

    cart: any;

    constructor(public dishService: DishService,
                private modalCtrl: ModalController, private  alertCtrl: AlertController) {
    }

    ngOnInit() {
        this.cart = this.dishService.cart;
    }

    decreaseCartItem(dish) {
        this.dishService.totalItems -= 1;
        for (let [index, d] of this.cart.entries()) {
            if (d.UuidDish === dish.UuidDish) {
                d.dish.Unit -= 1;
                if (d.dish.Unit === 0) {
                    this.cart.splice(index, 1);
                }
            }
        }
    }

    increaseCartItem(dish) {
        this.dishService.totalItems += 1;

        let added = false;
        for (let d of this.cart) {
            if (d.UuidDish === dish.UuidDish) {
                d.dish.Unit += 1;
                added = true;
                break;
            }
        }
        if (!added) {
            dish.dish.Unit += 1;
            this.cart.push(dish);
            this.dishService.pushOnCart(this.cart);
        }
    }

    removeCartItem(dish) {
        for (let [index, d] of this.cart.entries()) {
            if (d.UuidDish === dish.UuidDish) {
                this.dishService.totalItems = this.dishService.totalItems - d.dish.Unit;
                this.cart.splice(index, 1);
            }
        }
    }

    getTotal() {
        return this.cart.reduce((i, j) => i + j.dish.Price * j.dish.Unit, 0);
    }

    close() {
        this.modalCtrl.dismiss();
    }


    async checkout() {
        this.dishService.sendPrenotation();
    }
}

