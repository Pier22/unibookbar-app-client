import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePlacePage } from './home-place.page';

const routes: Routes = [
  {
    path: '',
    component: HomePlacePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePlacePageRoutingModule {}
