import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-home-place',
    templateUrl: './home-place.page.html',
    styleUrls: ['./home-place.page.scss'],
})
export class HomePlacePage implements OnInit {

    img_place = 'assets/img/place.png';

    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    public goTo(path: string) {
        this.router.navigate([path],{replaceUrl:true});
    }

}
