import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomePlacePage } from './home-place.page';

describe('HomePlacePage', () => {
  let component: HomePlacePage;
  let fixture: ComponentFixture<HomePlacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePlacePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePlacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
