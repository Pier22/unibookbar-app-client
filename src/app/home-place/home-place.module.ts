import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePlacePageRoutingModule } from './home-place-routing.module';
import { HomePlacePage } from './home-place.page';
import {MenuClass} from "../menu.module";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomePlacePageRoutingModule,
        MenuClass
    ],
  declarations: [HomePlacePage]
})
export class HomePlacePageModule {}
