import {Component, OnInit} from '@angular/core';
import {ModalController} from "@ionic/angular";
import {DishService} from "../services/dish.service";

@Component({
    selector: 'app-dish-details',
    templateUrl: './dish-details.page.html',
    styleUrls: ['./dish-details.page.scss'],
})
export class DishDetailsPage implements OnInit {

    img_dish = 'assets/img/carbonara.jpg';

    constructor(public modalController: ModalController, public dishService: DishService) {
    }

    ngOnInit() {
    }

    selectStar(star, id_button): boolean {
        star = (Math.round(star / 1));
        return star == id_button;
    }
}
