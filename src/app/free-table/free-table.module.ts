import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FreeTablePageRoutingModule } from './free-table-routing.module';

import { FreeTablePage } from './free-table.page';
import {MenuClass} from "../menu.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FreeTablePageRoutingModule,
        MenuClass
    ],
  declarations: [FreeTablePage]
})
export class FreeTablePageModule {}
