import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FreeTablePage } from './free-table.page';

describe('FreeTablePage', () => {
  let component: FreeTablePage;
  let fixture: ComponentFixture<FreeTablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeTablePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FreeTablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
