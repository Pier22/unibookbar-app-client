import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {TableService} from "../services/table.service";

@Component({
    selector: 'app-free-table',
    templateUrl: './free-table.page.html',
    styleUrls: ['./free-table.page.scss'],
})
export class FreeTablePage implements OnInit {

    constructor(private router: Router, public table_service: TableService) {
    }

    ngOnInit() {
        this.table_service.getFreeTable()
    }

    public goTo(path: string) {
        this.router.navigate([path],{replaceUrl:true});
    }

    sendReservationTable(UuidTable: string) {
        this.table_service.sendReservationTable(UuidTable);
        for (let i = 0; i < this.table_service.data.length; i++) {
            if (this.table_service.data[i]['UuidTable'] == UuidTable) {
                this.table_service.data.splice(0, 1);
            }
        }
    }
}
