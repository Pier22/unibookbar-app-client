import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FreeTablePage } from './free-table.page';

const routes: Routes = [
  {
    path: '',
    component: FreeTablePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FreeTablePageRoutingModule {}
