import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private router: Router) {
    }

    canActivate() {
        if (localStorage.getItem('ACCESS_TOKEN')) {
            return true;
        }
        this.router.navigate(['/login'],{replaceUrl:true});
        return false;
    }

}
