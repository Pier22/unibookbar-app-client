import {NgModule} from "@angular/core";
import {SidemenuComponent} from './sidemenu/sidemenu.component';
import {IonicModule} from "@ionic/angular";
import {RouterModule} from "@angular/router";

import {CommonModule} from '@angular/common';

@NgModule({
    declarations: [SidemenuComponent],
    imports: [
        IonicModule,
        RouterModule,
        CommonModule
    ],
    exports: [SidemenuComponent]
})

export class MenuClass {

}
